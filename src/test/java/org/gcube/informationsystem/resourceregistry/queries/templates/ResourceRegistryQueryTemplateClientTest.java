package org.gcube.informationsystem.resourceregistry.queries.templates;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;

import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.com.fasterxml.jackson.databind.node.ObjectNode;
import org.gcube.informationsystem.base.reference.IdentifiableElement;
import org.gcube.informationsystem.model.reference.entities.Entity;
import org.gcube.informationsystem.model.reference.properties.Metadata;
import org.gcube.informationsystem.queries.templates.impl.entities.QueryTemplateImpl;
import org.gcube.informationsystem.queries.templates.impl.properties.TemplateVariableImpl;
import org.gcube.informationsystem.queries.templates.reference.entities.QueryTemplate;
import org.gcube.informationsystem.queries.templates.reference.properties.TemplateVariable;
import org.gcube.informationsystem.resourceregistry.api.exceptions.AlreadyPresentException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.serialization.ElementMapper;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ResourceRegistryQueryTemplateClientTest extends ContextTest {
	
	private static Logger logger = LoggerFactory.getLogger(ResourceRegistryQueryTemplateClientTest.class);
	
	public static final String QUERY_TEMPLATE_NAME = "Test";
	public static final String QUERY_TEMPLATE_DESCRIPTION = "A Test Query Template";
	public static final String QUERY_TEMPLATE_DESCRIPTION_UPDATED = "A Test Query Template UPDATED";
	
	public static final String STATE_VARIABLE_NAME = "$state";
	public static final String NAME_VARIABLE_NAME = "$name";
	public static final String GROUP_VARIABLE_NAME = "$group";
	
	protected ResourceRegistryQueryTemplateClient resourceRegistryQueryTemplateClient;
	
	public ResourceRegistryQueryTemplateClientTest() {
		if(ContextTest.RESOURCE_REGISTRY_URL !=null  && !ContextTest.RESOURCE_REGISTRY_URL.isEmpty()) {
			resourceRegistryQueryTemplateClient = new ResourceRegistryQueryTemplateClientImpl(ContextTest.RESOURCE_REGISTRY_URL);
		}else {
			resourceRegistryQueryTemplateClient = ResourceRegistryQueryTemplateClientFactory.create();
		}
	}
	
	public static void checkUUIDAndMetadata(IdentifiableElement previous, IdentifiableElement got) {
		Metadata gotMetadata = got.getMetadata();
		Metadata previousMetadata = previous.getMetadata();
		
		Assert.assertTrue(gotMetadata != null);
		Assert.assertTrue(got.getID() != null);
		
		String user = ContextTest.getUser();
		Assert.assertTrue(gotMetadata.getLastUpdateBy().compareTo(user) == 0);
		
		if(previous.getID()!=null) {
			Assert.assertTrue(got.getID().compareTo(previous.getID()) == 0);
		}
		
		if(previousMetadata != null) {
			Assert.assertTrue(gotMetadata.getCreatedBy().compareTo(user) == 0);
			Assert.assertTrue(gotMetadata.getCreatedBy().compareTo(previousMetadata.getCreatedBy()) == 0);
			Assert.assertTrue(gotMetadata.getCreationTime().compareTo(previousMetadata.getCreationTime()) == 0);
		} 
		
		Assert.assertFalse(gotMetadata.getCreationTime().after(gotMetadata.getLastUpdateTime()));
		
	}
	
	protected void assertions(QueryTemplate expected, QueryTemplate got) {
		checkUUIDAndMetadata(expected, got);
		Assert.assertTrue(expected.getName().compareTo(got.getName()) == 0);
		Assert.assertTrue(expected.getDescription().compareTo(got.getDescription()) == 0);
		// Template and TemplateVariable should be compared
	}
	
	protected QueryTemplate create(QueryTemplate queryTemplate) throws ResourceRegistryException, IOException {
		list();
		QueryTemplate createdQueryTemplate = resourceRegistryQueryTemplateClient.create(queryTemplate);
		assertions(queryTemplate, createdQueryTemplate);
		list(createdQueryTemplate);
		return createdQueryTemplate;
	}

	protected QueryTemplate read(QueryTemplate queryTemplate) throws ResourceRegistryException, IOException {
		list(queryTemplate);
		QueryTemplate readQueryTemplate = resourceRegistryQueryTemplateClient.read(queryTemplate);
		assertions(queryTemplate, readQueryTemplate);
		return queryTemplate;
	}
	
	protected void run(QueryTemplate queryTemplate) throws Exception {
		
		String ret = resourceRegistryQueryTemplateClient.runGetString(queryTemplate.getName());
		logger.debug("Run result is {}", ret);
		
		List<Entity> entities = resourceRegistryQueryTemplateClient.run(queryTemplate.getName());
		logger.debug("Run result is {}", entities);
		
		entities = resourceRegistryQueryTemplateClient.run(queryTemplate);
		logger.debug("Run result is {}", entities);
		
		ObjectMapper objectMapper = new ObjectMapper();
		ObjectNode params = objectMapper.createObjectNode();
		params.put(STATE_VARIABLE_NAME, "running");
		params.put(GROUP_VARIABLE_NAME, "DataAccess");
		params.put(NAME_VARIABLE_NAME, "StorageHub");
		
		ret = resourceRegistryQueryTemplateClient.run(queryTemplate.getName(), objectMapper.writeValueAsString(params));
		logger.debug("Run result is {}", ret);
		
		entities = resourceRegistryQueryTemplateClient.run(queryTemplate.getName(), params);
		logger.debug("Run result is {}", entities);
		
		entities = resourceRegistryQueryTemplateClient.run(queryTemplate, params);
		logger.debug("Run result is {}", entities);
	}
	
	protected QueryTemplate update(QueryTemplate queryTemplate) throws ResourceRegistryException, IOException {
		list(queryTemplate);
		
		queryTemplate.setDescription(QUERY_TEMPLATE_DESCRIPTION_UPDATED);
		QueryTemplate updatedQueryTemplate = resourceRegistryQueryTemplateClient.update(queryTemplate);
		assertions(queryTemplate, updatedQueryTemplate);
		list(updatedQueryTemplate);
		
		// Restoring Previous description on queryTemplate
		queryTemplate.setDescription(QUERY_TEMPLATE_DESCRIPTION);
		return updatedQueryTemplate;
	}
	
	protected boolean delete(QueryTemplate queryTemplate) throws ResourceRegistryException {
		list(queryTemplate);
		boolean deleted = resourceRegistryQueryTemplateClient.delete(queryTemplate);
		Assert.assertTrue(deleted);
		logger.debug("Deleted {} with name {}", QueryTemplate.NAME, queryTemplate.getName());
		list();
		return deleted;
	}
	
	protected void list(QueryTemplate...queryTemplates) throws ResourceRegistryException {
		List<QueryTemplate> all = resourceRegistryQueryTemplateClient.all();
		if(queryTemplates==null) {
			Assert.assertTrue(all.size()==0);
			return;
		}
		Assert.assertTrue(all.size()==queryTemplates.length);
		for(QueryTemplate expected : queryTemplates) {
			String expectedName = expected.getName();
			boolean found = false;
			for(QueryTemplate got : all) {
				String gotName = got.getName();
				if(gotName.compareTo(expectedName)==0) {
					found = true;
					assertions(expected, got);
					break;
				}
			}
			Assert.assertTrue(found);
		}
	}
	
	protected void invalidCreate(QueryTemplate queryTemplate) throws ResourceRegistryException, IOException {
		try {
			QueryTemplate recreatedQueryTemplate = create(queryTemplate);
			throw new RuntimeException(ElementMapper.marshal(recreatedQueryTemplate) + " was created successfully. This is not what we expected");
		} catch(AlreadyPresentException e) {
			logger.debug("As expected {} cannot be created.", ElementMapper.marshal(queryTemplate));
		}
	}
	
	public File getQueryTemplatesDirectory() throws Exception {
		URL logbackFileURL = ContextTest.class.getClassLoader().getResource("logback-test.xml");
		File logbackFile = new File(logbackFileURL.toURI());
		File resourcesDirectory = logbackFile.getParentFile();
		return new File(resourcesDirectory, "queryTemplates");
	}
	
	protected QueryTemplate getQueryTemplate(String name, JsonNode jsonNode) {
		QueryTemplate queryTemplate = new QueryTemplateImpl();
		queryTemplate.setName(name);
		queryTemplate.setDescription(QUERY_TEMPLATE_DESCRIPTION);
		queryTemplate.setTemplate(jsonNode);
		
		TemplateVariable stateTemplateVariable = new TemplateVariableImpl();
		stateTemplateVariable.setName(STATE_VARIABLE_NAME);
		stateTemplateVariable.setDescription("StateFacet value");
		stateTemplateVariable.setDefaultValue("running");
		queryTemplate.addTemplateVariable(stateTemplateVariable);
		
		TemplateVariable nameTemplateVariable = new TemplateVariableImpl();
		nameTemplateVariable.setName(NAME_VARIABLE_NAME);
		nameTemplateVariable.setDescription("SoftwareFacet name");
		nameTemplateVariable.setDefaultValue("resource-registry");
		queryTemplate.addTemplateVariable(nameTemplateVariable);
		
		TemplateVariable groupTemplateVariable = new TemplateVariableImpl();
		groupTemplateVariable.setName(GROUP_VARIABLE_NAME);
		groupTemplateVariable.setDescription("SoftwareFacet group");
		groupTemplateVariable.setDefaultValue("information-system");
		queryTemplate.addTemplateVariable(groupTemplateVariable);
		
		return queryTemplate;
	}
	
	protected JsonNode getTemplate() throws Exception {
		File queryTemplatesDirectory = getQueryTemplatesDirectory();
		File jsonQueryTemplateFile = new File(queryTemplatesDirectory, "queryTemplate" + 1 + ".json");
		ObjectMapper objectMapper = new ObjectMapper();
		JsonNode jsonNode = objectMapper.readTree(jsonQueryTemplateFile);
		return jsonNode;
	}
	
	protected QueryTemplate getQueryTemplate() throws Exception {
		JsonNode jsonNode = getTemplate();
		QueryTemplate queryTemplate = getQueryTemplate(QUERY_TEMPLATE_NAME, jsonNode);
		String json = ElementMapper.marshal(queryTemplate);
		logger.info("Marshalled {} - {}", QueryTemplate.NAME, json);
		return queryTemplate;
	}
	
	// @Test
	public void readTest() throws Exception {
		QueryTemplate readQueryTemplate = resourceRegistryQueryTemplateClient.read(QUERY_TEMPLATE_NAME);
		String json = ElementMapper.marshal(readQueryTemplate);
		logger.info("Read {}: {}", QueryTemplate.NAME, json);
	}
	
	@Test
	public void completeTest() throws Exception {
		try {
			QueryTemplate queryTemplate = getQueryTemplate();
						
			QueryTemplate createdQueryTemplate = create(queryTemplate);
			
			QueryTemplate readQueryTemplate = read(createdQueryTemplate);
			
			QueryTemplate updatedQueryTemplate = update(readQueryTemplate);
			
			run(updatedQueryTemplate);
			
			QueryTemplate readUpdatedQueryTemplate = read(updatedQueryTemplate);
			
			delete(readUpdatedQueryTemplate);
		}catch (Throwable e) {
			resourceRegistryQueryTemplateClient.delete(QUERY_TEMPLATE_NAME);
		}
		logger.debug("The DB should be now clean");
	}
	
}
