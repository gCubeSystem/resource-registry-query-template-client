package org.gcube.informationsystem.resourceregistry.queries.templates;

import java.util.List;

import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.informationsystem.model.reference.ERElement;
import org.gcube.informationsystem.queries.templates.reference.entities.QueryTemplate;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.queries.templates.QueryTemplateAlreadyPresentException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.queries.templates.QueryTemplateNotFoundException;
import org.gcube.informationsystem.resourceregistry.api.request.RequestInfo;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface ResourceRegistryQueryTemplateClient extends RequestInfo {

	public void addHeader(String name, String value);
	
	public List<QueryTemplate> all() throws ResourceRegistryException;
	
	public QueryTemplate create(QueryTemplate queryTemplate) throws QueryTemplateAlreadyPresentException, ResourceRegistryException;

	public String create(String queryTemplate) throws QueryTemplateAlreadyPresentException, ResourceRegistryException;

	public boolean exist(QueryTemplate queryTemplate) throws ResourceRegistryException;

	public boolean exist(String queryTemplateName) throws ResourceRegistryException;
		
	public QueryTemplate read(QueryTemplate queryTemplate) throws QueryTemplateNotFoundException, ResourceRegistryException;

	public QueryTemplate read(String queryTemplateName) throws QueryTemplateNotFoundException, ResourceRegistryException;

	public String readAsString(String queryTemplateName) throws QueryTemplateNotFoundException, ResourceRegistryException;

	public QueryTemplate update(QueryTemplate queryTemplate) throws QueryTemplateNotFoundException, ResourceRegistryException;

	public String update(String queryTemplate) throws QueryTemplateNotFoundException, ResourceRegistryException;

	public String runGetString(String name) throws QueryTemplateNotFoundException, ResourceRegistryException;
	
	public <E extends ERElement> List<E> run(String name) throws QueryTemplateNotFoundException, ResourceRegistryException;
	
	public <E extends ERElement> List<E> run(QueryTemplate queryTemplate) throws QueryTemplateNotFoundException, ResourceRegistryException;
	
	public String run(String name, String params) throws QueryTemplateNotFoundException, ResourceRegistryException;
	
	public <E extends ERElement> List<E> run(String name, JsonNode jsonNode) throws QueryTemplateNotFoundException, ResourceRegistryException;
	
	public <E extends ERElement> List<E> run(QueryTemplate queryTemplate, JsonNode jsonNode) throws QueryTemplateNotFoundException, ResourceRegistryException;
	
	public boolean delete(QueryTemplate queryTemplate) throws QueryTemplateNotFoundException, ResourceRegistryException;

	public boolean delete(String queryTemplateName) throws QueryTemplateNotFoundException, ResourceRegistryException;

}
