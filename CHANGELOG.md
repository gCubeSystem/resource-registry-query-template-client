This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for Resource Registry Query Template Client

## [v1.2.0-SNAPSHOT]

- Added support for paginated results [#24648]


## [v1.1.1]

- Migrated code to reorganized E/R format [#24992]


## [v1.1.0]

- Fixed result of run from List<Entity> to List<ERElement>
- Enhanced gcube-bom version
- Added usage of common-utility to overcome issues with different Smartgears version (i.e. 3 and 4)
- Added the possibility for a client to add additional HTTP headers
- Added the possibility to create a client instance by specifying context


## [v1.0.0]

- First Release

